import React from 'react'
import PropTypes from 'prop-types'
import Item from './Item'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        maxWidth: '800px',
        display: 'grid',
        gridTemplateColumns: 'repeat(auto-fill, minmax(200px, 1fr))',
        margin: 'auto',
    },
  }));

function List(props) {
    const classes = useStyles();
    const renderList = (items) => {
        if(!items) return <p>Opps, nothing to see here.</p>
        return items.map( product => <Item key={product.id} bike={product} />);
    }

    return (
        <div className={classes.root}>
            {renderList(props.products)}
        </div>
       
    )
}

List.propTypes = {
    products: PropTypes.array.isRequired
}

export default List

