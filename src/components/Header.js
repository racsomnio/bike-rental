import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import Badge from '@material-ui/core/Badge';
import Checkout from './Checkout'
import useGlobal from '../store';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  emoji:{
      background: '#fff',
      borderRadius: '50%',
      padding: '0.5rem',
  },
  title: {
    flexGrow: 1,
  },
  cartText:{
      fontSize: '0.8rem',
  },
  dialog:{
      padding: '0.5rem',
  }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

export default function ButtonAppBar() {
    const classes = useStyles();
    const [globalState] = useGlobal();
    let total = globalState.cart.reduce( (a,b) => a + (b.price * b.count), 0);
    console.log(globalState.cart)
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    
    return (
        <div className={classes.root}>
            <AppBar position="fixed">
                <Toolbar>
                <Typography variant="h6" className={classes.title}>
                <span className={classes.emoji} role="img" aria-label="Bike">🚴‍♂️</span> Bikes
                </Typography>
                
                <IconButton aria-label="show items" color="inherit" onClick={handleClickOpen}>
                    {total !== 0 ? <span className={classes.cartText}>Review your cart</span>: null}
                    <Badge badgeContent={(total !== 0) ? `$${total}` : null} color="secondary">
                        <ShoppingCartIcon />
                    </Badge>
                </IconButton>
                </Toolbar>
            </AppBar>
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogContent className={classes.dialog}>
                    <Checkout updateQty={false}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Continue
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}