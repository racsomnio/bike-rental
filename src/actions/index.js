export const addToCart = (store, item) => {
  const found = store.state.cart.find(element => element.id === item.id);
  if(!found){
    var cart = [...store.state.cart, item];
  }
  else{
    if(item.count === 0){
      const deleteZero = store.state.cart.filter( el => el.id !== item.id);
      var cart= [...deleteZero];
    }else{
      var cart = [...store.state.cart];
    }
  }
  
  store.setState({ cart });
};

export const deleteFromCart = (store, id) => {
  const deleteOne = store.state.cart.filter(item => item.id !== id);

  store.setState({ cart: deleteOne });
};

export const increaseQty = (store, id) => {
  const found = store.state.cart.find(element => element.id === id);
  found.count += 1;
  const cart = [...store.state.cart]

  store.setState({ cart });
};

export const decreaseQty = (store, id) => {
  const found = store.state.cart.find(element => element.id === id);
  found.count = found.count===1 ? 1 : found.count - 1;
  const cart = [...store.state.cart]

  store.setState({ cart });
};

export const resetCart = (store) => {
  store.setState({ cart: [] });
};