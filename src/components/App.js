import React from 'react'
import PropTypes from 'prop-types'
import Header from './Header'
import Container from './Container'

// import useGlobal from '../store';


function App(props) {
    const {products} = props.data;


    return (
        <>
            <Header/>
            <Container products={products}/>
        </>
    );
}

App.propTypes = {
    data: PropTypes.object.isRequired
}

export default App

