import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import useGlobal from '../store';

const useStyles = makeStyles(theme => ({
    root:{
        borderTop: '1px dashed #ccc',
        textAlign: 'center',
        padding: '1rem',
    },
}));

export default function Total() {
    const [globalState] = useGlobal();
    const classes = useStyles();
    return (
        <div className={classes.root}>
            Total: ${globalState.cart.reduce( (a,b) => a + (b.price * b.count), 0)}
        </div>
    )
}
