import React from 'react'
import PropTypes from 'prop-types'
import useGlobal from '../store';
import { makeStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';
import Chip from '@material-ui/core/Chip';

const useStyles = makeStyles(theme => ({
    root: {
        border: '1px solid #ccc',
        borderRadius: '5px',
        margin: '0.5rem',
        fontSize: '0.8rem',
        textAlign: 'center',
    },
    img:{
        width: '100%',
        borderRadius: '5px 5px 0 0',
    },
    h2:{
        fontSize: '0.8rem',
        width: '80%',
        margin: '0.5rem auto',
    },
    slider:{
         width: '80%',
         margin: '2rem auto',
     }
  }));

function Bike(props) {
    const classes = useStyles();
    const { image, name, price } = props.bike;
    const [globalState, globalActions] = useGlobal();

    const marks = [
        {
          value: 1,
          label: '1',
        },
        {
          value: 2,
          label: '2',
        },
        {
          value: 3,
          label: '3',
        },
        {
          value: 4,
          label: '4',
        },
        {
            value: 5,
            label: '5',
        },
        {
            value: 6,
            label: '6',
        },
        {
            value: 7,
            label: '7',
        },
        {
            value: 8,
            label: '8',
        },
        {
            value: 9,
            label: '9',
        },
        {
            value: 10,
            label: '10',
        },
    ];

    return (
        <div className={classes.root}>
            <img src={image} alt={name} className={classes.img} />
            <h2 className={classes.h2}>{name}</h2>
            <Chip label={`$${price}`}/>
            <div className={classes.slider}>
                <Slider
                    defaultValue={0}
                    color= "secondary"
                    aria-labelledby="discrete-slider-always"
                    step={1}
                    marks={marks}
                    valueLabelDisplay="on"
                    max={10}
                    min={0}        
                    onChangeCommitted={(e, val) => {
                        props.bike.count=val
                        globalActions.addToCart(props.bike);
                    }}
                />
                <div>How many?</div>
            </div>
        </div>
    )
}

Bike.propTypes = {
    bike: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
    })
}

export default Bike

