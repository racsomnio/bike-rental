import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import CheckoutItem from './CheckoutItem';
import Total from './Total';

import useGlobal from '../store';
// import PropTypes from 'prop-types'

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      maxWidth: 752,
      margin: 'auto',
    },
    demo: {
      backgroundColor: theme.palette.background.paper,
    },
    title: {
      margin: theme.spacing(4, 0, 2),
    },
  }));

function Checkout(props) {
    const classes = useStyles();
    const [dense] = React.useState(false);
    const [globalState] = useGlobal();

    function generate(element) {
        if(globalState.cart.length === 0) return <p>Nothing here.</p>;
        
        return globalState.cart.map(value =>
          React.cloneElement(element, {
            key: value.id,
            item: value,
          }),
        );
    }

    return (
        <Grid item xs={12}>
          <div className={classes.demo}>
            <List dense={dense}>
              {generate(<CheckoutItem updateQty={props.updateQty}/>)}
            </List>
            <Total/>
          </div>
        </Grid>
    )
}

export default Checkout

