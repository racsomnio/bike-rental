import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from './List'
import Checkout from './Checkout'
import Snackbar from '@material-ui/core/Snackbar';
import useGlobal from '../store';

const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
      maxWidth: '800px',
      margin: '5rem auto 2rem',
    },
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: theme.spacing(2),
      display: 'flex',
      justifyContent: 'space-between',
    },
    resetContainer: {
      padding: theme.spacing(3),
    },
  }));

function Container (props) {
    const bikes = props.products.filter( product => product.product_type === 'bike' );
    const accesories = props.products.filter( product => product.product_type === 'accessory');
    const addons = props.products.filter( product => product.product_type === 'addon');
    const [globalState, globalActions] = useGlobal();

    // console.log(bikes, accesories, addons)

    const classes = useStyles();
    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();

    const [state, setState] = React.useState({
        open: false,
      });
    
      const { open } = state;
    
      const handleClick = newState => () => {
        setState({ open: true});
      };
    
      const handleClose = () => {
        setState({ open: false });
      };

    const handleNext = () => {
        const isThereABike = globalState.cart.find(element => element.product_type === 'bike');
        if(activeStep === steps.length - 1 && (!isThereABike)) {
            handleClick()();
            return;
        }

        if(activeStep === steps.length - 1) globalActions.resetCart();
        setActiveStep(prevActiveStep => prevActiveStep + 1);
        
    };

    const handleBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };

    function getSteps() {
        return ['Choose a Bike', 'Accesories', 'Add on', 'Check Out'];
    }

    function getStepContent(step) {
        switch (step) {
        case 0:
            return <List products={bikes}/>;
        case 1:
            return <List products={accesories}/>;
        case 2:
            return <List products={addons}/>;
        default:
            return <Checkout/>;
        }
    }

    return (
        <div className={classes.root}>
            <Stepper activeStep={activeStep} orientation="vertical">
                {steps.map((label, index) => (
                <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                    <StepContent>
                    {getStepContent(index)}
                    <div className={classes.actionsContainer}>
                    
                        <Button
                            disabled={activeStep === 0}
                            onClick={handleBack}
                            className={classes.button}
                        >
                            Back
                        </Button>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={handleNext}
                            className={classes.button}
                        >
                            {activeStep === steps.length - 1 ? 'Pay' : 'Next'}
                        </Button>
                    </div>
                    </StepContent>
                </Step>
                ))}
            </Stepper>
            {activeStep === steps.length && (
                <Paper square elevation={0} className={classes.resetContainer}>
                <Typography>You&apos;re all set - your journey is about to start. You will get a mail soon.</Typography>
                <Button onClick={handleReset}>
                    Rent More?
                </Button>
                </Paper>
            )}
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                key="top, center"
                open={open}
                onClose={handleClose}
                message="Opps, Please go back to step 1 and select a bike"
                autoHideDuration={5000}
            />
        </div>
    );
}

export default Container