import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';
import BackspaceIcon from '@material-ui/icons/Backspace';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import useGlobal from '../store';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
    root:{
        fontSize: '1rem',
        maxWidth: '320px',
        padding: '0',
        marginBottom: '2rem',
    },
    container:{
        justifyContent: 'flex-end'
    },
    listItemAvatar:{
        minWidth: '35px',
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
        margin: theme.spacing(1),
    },
    iconButton:{
        fontSize: 'inherit',
    },
    delete:{
        position: 'absolute',
        top: '-15px',
        left: '5px',
    },
    image:{
        borderRadius: '10px',
    },
}));

function CheckoutItem(props) {
    const { name, image, price, count, id } = props.item;
    const [globalState, globalActions] = useGlobal();
    const classes = useStyles();
    // console.log(props);
    
    function Qty() {
        if (props.updateQty) {
          return (
            <>
                <IconButton 
                    aria-label="delete"
                    onClick={() => globalActions.decreaseQty(id)}
                >
                    <RemoveCircleIcon/>
                </IconButton>
                {count}
                <IconButton 
                    aria-label="delete"
                    onClick={() => globalActions.increaseQty(id)}
                >
                    <AddCircleIcon/>
                </IconButton>
            </>
          )
        }else{
            return <span>{count}</span>;
        }
    }

    function Del(){
        if (props.updateQty) {
            return (
                <IconButton 
                    edge="end" 
                    aria-label="delete"
                    onClick={() => globalActions.deleteFromCart(id)}
                    className={classes.delete}
                >
                    <BackspaceIcon />
                </IconButton>
            )
        }else{
            return null;
        }
    }

    return (
        <ListItem className={classes.root}>
            <Grid container spacing={1} className={classes.container}>
                <Grid item xs={3}>
                    <img src={image} alt={name} width="100%" className={classes.image} />
                </Grid>
                <Grid item xs={8}>
                    <Grid item xs={12}>
                        <div>{name}</div>
                        <div><strong><small>Qty: </small></strong>
                             
                            <Qty/>
                            
                        </div>
                        <div><strong><small>{`Item${count===1 ? '' : 's'} total: `}</small></strong>{`$${price * count}`}</div>
                    </Grid>
                    <Del/>
                        
                </Grid>
            </Grid>
        </ListItem>
    )
}

CheckoutItem.defaultProps = {
    updateQty: true,
};

export default CheckoutItem

